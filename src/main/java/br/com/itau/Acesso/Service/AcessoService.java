package br.com.itau.Acesso.Service;

import br.com.itau.Acesso.Client.AcessoCliente;
import br.com.itau.Acesso.Client.AcessoPorta;
import br.com.itau.Acesso.Client.Cliente;
import br.com.itau.Acesso.Client.Porta;
import br.com.itau.Acesso.DTO.AcessoEntradaDTO;
import br.com.itau.Acesso.DTO.AcessoSaidaDTO;
import br.com.itau.Acesso.Exceptions.AcessoJaCadastrado;
import br.com.itau.Acesso.Exceptions.AcessoNaoAutorizado;
import br.com.itau.Acesso.Model.Acesso;
import br.com.itau.Acesso.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {


    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    AcessoCliente acessoCliente;

    @Autowired
    AcessoPorta acessoPorta;

    @Autowired
    private KafkaTemplate<Object, Acesso> producer;

    public AcessoSaidaDTO incluirNovoAcessoAPorta(AcessoEntradaDTO acessoEntradaDTO) {

        Optional<Acesso> optionalAcesso = acessoRepository.findByClienteidAndPortaid(acessoEntradaDTO.getCliente_id(), acessoEntradaDTO.getPorta_id());


        if (!optionalAcesso.isPresent()) {


            Cliente cliente = acessoCliente.buscarClientePeloId(acessoEntradaDTO.getCliente_id());
            Porta porta = acessoPorta.buscarPortaPeloId(acessoEntradaDTO.getPorta_id());

            Acesso acesso = new Acesso();

            acesso.setClienteid(cliente.getId());
            acesso.setPortaid(porta.getId());

            enviarAoKafka(acesso, true);


            acessoRepository.save(acesso);


            return new AcessoSaidaDTO(acesso);
        } else {
            throw new AcessoJaCadastrado();
        }

    }

    public AcessoSaidaDTO buscarAcessoPorId(int clienteid, int portaid) {

        Optional<Acesso> optionalAcesso = acessoRepository.findByClienteidAndPortaid(clienteid, portaid);

        if (optionalAcesso.isPresent()) {

            //Enviar os dados ao KAFCA

            Acesso acesso = optionalAcesso.get();

            enviarAoKafka(acesso, true);

            return new AcessoSaidaDTO(acesso);

        } else {

            Acesso acesso = new Acesso();
            acesso.setPortaid(portaid);
            acesso.setPortaid(clienteid);

            enviarAoKafka(acesso, false);
            throw new AcessoNaoAutorizado();


        }

    }


    public void enviarAoKafka(Acesso acesso, Boolean autorizado) {


        acesso.setAcessoautorizado(autorizado);
        producer.send("spec4-anderson-fiuza-2", acesso);
    }


    public void excluirAcesso(int clienteid, int portaid) {


        acessoRepository.removeAllByClienteidAndPortaid(clienteid, portaid);

    }
}
