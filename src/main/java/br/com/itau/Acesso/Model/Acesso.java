package br.com.itau.Acesso.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Acesso {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @NotNull(message = "Favor informar a porta")
    private int  portaid;

    @NotNull(message = "Favor informar o cliente")
    private int clienteid;

    private Boolean acessoautorizado = false ;

    public Boolean getAcessoautorizado() {
        return acessoautorizado;
    }


    public void setAcessoautorizado(Boolean acessoautorizado) {
        this.acessoautorizado = acessoautorizado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPortaid() {
        return portaid;
    }

    public void setPortaid(int portaid) {
        this.portaid = portaid;
    }

    public int getClienteid() {
        return clienteid;
    }

    public void setClienteid(int clienteid) {
        this.clienteid = clienteid;
    }
}
