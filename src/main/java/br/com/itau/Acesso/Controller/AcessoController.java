package br.com.itau.Acesso.Controller;

import br.com.itau.Acesso.DTO.AcessoEntradaDTO;
import br.com.itau.Acesso.DTO.AcessoSaidaDTO;
import br.com.itau.Acesso.Model.Acesso;
import br.com.itau.Acesso.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoSaidaDTO incluirNovoAcesso(@RequestBody @Valid AcessoEntradaDTO acessoEntradaDTO) {

        return acessoService.incluirNovoAcessoAPorta(acessoEntradaDTO);

    }

    @GetMapping(value = "/{clienteid}/{portaid}")
    @ResponseStatus(HttpStatus.OK)
    public AcessoSaidaDTO consultaAcesoAoSistema(@PathVariable(value = "clienteid") int clienteid, @PathVariable(value = "portaid") int portaid){

        return  acessoService.buscarAcessoPorId(clienteid,portaid);

    }

    @PutMapping(value = "/DELETE/{clienteid}/{portaid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesoAoSistema(@PathVariable(value = "clienteid") int clienteid, @PathVariable(value = "portaid") int portaid){

         acessoService.excluirAcesso(clienteid,portaid);

    }


}
