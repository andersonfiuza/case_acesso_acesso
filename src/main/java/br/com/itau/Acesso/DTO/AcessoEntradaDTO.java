package br.com.itau.Acesso.DTO;

import br.com.itau.Acesso.Model.Acesso;

public class AcessoEntradaDTO {



    private int cliente_id;
    private int porta_id;

    public int getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(int cliente_id) {
        this.cliente_id = cliente_id;
    }

    public int getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(int porta_id) {
        this.porta_id = porta_id;
    }


}
