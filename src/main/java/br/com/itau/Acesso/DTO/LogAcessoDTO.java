package br.com.itau.Acesso.DTO;

import br.com.itau.Acesso.Model.Acesso;

public class LogAcessoDTO {


    private int id;
    private int  portaid;
    private int clienteid;

    private Boolean acessoautorizado = false ;

    public Boolean getAcessoautorizado() {
        return acessoautorizado;
    }

    public void setAcessoautorizado(Boolean acessoautorizado) {
        this.acessoautorizado = acessoautorizado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPortaid() {
        return portaid;
    }

    public void setPortaid(int portaid) {
        this.portaid = portaid;
    }

    public int getClienteid() {
        return clienteid;
    }

    public void setClienteid(int clienteid) {
        this.clienteid = clienteid;
    }

    public LogAcessoDTO(Acesso acesso) {

        this.setClienteid(acesso.getClienteid());
        this.setPortaid(acesso.getId());




    }

}
