package br.com.itau.Acesso.Client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="CLIENTE",configuration = AcessoClienteConfiguracao.class)
public interface AcessoCliente {

    @GetMapping("/{id}")
    Cliente buscarClientePeloId(@PathVariable int id);


}
