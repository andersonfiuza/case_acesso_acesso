package br.com.itau.Acesso.Client;

import br.com.itau.Acesso.Exceptions.ClienteNaoEncontrado;
import br.com.itau.Acesso.Model.Acesso;
import br.com.itau.Acesso.Service.AcessoService;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;

public class ClienteCartaoDecoder implements ErrorDecoder {


    @Autowired
    AcessoService acessoService;



    private ErrorDecoder errorDecoder = new Default();


    @Override
    public Exception decode(String s, Response response){
        if(response.status() == 404){

            Acesso acesso = new Acesso();

            return new ClienteNaoEncontrado();


        }
        return errorDecoder.decode(s,response);

    }
}
