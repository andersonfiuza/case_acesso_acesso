package br.com.itau.Acesso.Client;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClientePortaFallback implements AcessoCliente {

    @Override
    public Cliente buscarClientePeloId(int id) {
        throw  new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço Indisponivel");

    }
}
