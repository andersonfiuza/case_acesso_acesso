package br.com.itau.Acesso.Client;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AcessoPortaFallback implements AcessoPorta{

    @Override
    public Porta buscarPortaPeloId(int id) {

        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE,"SERVIÇO INDISPONIVEL!");
    }
}
