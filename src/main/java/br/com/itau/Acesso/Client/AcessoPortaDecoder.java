package br.com.itau.Acesso.Client;

import br.com.itau.Acesso.Exceptions.AcessoPortaNaoEncontrado;
import feign.Response;
import feign.codec.ErrorDecoder;

public class AcessoPortaDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {


            return new AcessoPortaNaoEncontrado();

        }
        return errorDecoder.decode(s,response);

    }

}
