package br.com.itau.Acesso.Client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.sound.sampled.Port;

@FeignClient(name= "PORTA", configuration = AcessoPortaConfiguracao.class)
public interface AcessoPorta {

    @GetMapping("/{id}")
    Porta buscarPortaPeloId(@PathVariable int id);




}
