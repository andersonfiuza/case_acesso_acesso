package br.com.itau.Acesso.Client;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class AcessoClienteConfiguracao {

    @Bean
    public ErrorDecoder buscarClientePeloIdDecoder() {return new ClienteCartaoDecoder();}

    @Bean
    public Feign.Builder builder(){
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClientePortaFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
